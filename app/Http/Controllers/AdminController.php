<?php

namespace App\Http\Controllers;
use App\Models\User;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function auth(Request $request)
    {
    	$admin = User::where('name', $request['login'])->first();
    	return json_encode(['token' => $admin->remember_token]);
    }
}
