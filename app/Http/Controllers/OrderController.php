<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\EventType;

class OrderController extends Controller
{
    public function store(Request $request)
    {
        // $validated = $request->validate([
        //     'eventType' => 'numeric',
        //     'ownerLastName' => 'string|max:191',
        //     'ownerFirstName' => 'string|max:191',
        //     'coOwnerGender' => 'string|max:191',
        //     'coOwnerPhone' => 'string|max:191',
        //     'coOwnerLastName' => 'string|max:191',
        //     'coOwnerFirstName' => 'string|max:191',
        //     'hallName' => 'string|max:191',
        //     'eventDate' => 'string|max:191',
        //     'ownerGender' => 'string|max:191',
        //     'ownerPhone' => 'string|max:191',
        //     'numberOfGuests' => 'string|max:191',
        //     'emailFormControl' => 'string|max:191',
        //     'packageId' => 'numeric',
        //     'packageName' => 'string|max:191',
        //     'arrivalSms' => 'numeric',
        //     'reminderSms' => 'numeric',
        //     'thanksSms' => 'numeric',
        //     'creditLine' => 'numeric',
        //     'callFourthRounds' => 'numeric',
        //     'seatingSms' => 'numeric',
        //     'seatingAttendant' => 'numeric',
        //     'extraCalls' => 'numeric',
        //     'extraRoundSmsArrival' => 'numeric',
        //     'extraSmsSeating' => 'numeric',
        //     'extraTwoRoundsCalls' => 'numeric',
        //     'extraAttendant' => 'numeric',
        //     'callsRound' => 'numeric',
        //     'messagesRound' => 'numeric',
        //     'for' => 'string|max:191',
        //     'phone' => 'string|max:191',
        //     'email' => 'string|max:191|email',
        //     'idOwner' => 'numeric',
        //     'nameOwner' => 'string|max:191',
        //     'cardNumber' => 'string|max:191',
        //     'payments' => 'string|max:191',
        //     'cvv' => 'numeric',
        //     'validity' => 'string|max:191',
        // ]);

        dump((array)$request->query);

    	// $order = Order::create($request->except('submit'));

    	// $result['id'] = $order->id;
    	// $result['created_at'] = $order->created_at;

        // foreach($request->except('submit') as $key => $value){
        //     if($value){
        //         $result[$key] = $value;
        //     }
        // }

    	//return response()->json($result);
    }

    public function event()
    {
        $types = EventType::select('id', 'eventType')->get();
        return response()->json($types);
    }

    public function faq()
    {
        header('Content-type: application/json');   
        $array = [];
  
        $array[] = [
            'id' => 1,
            'question' => 'What is arrival SMS?',
            'answer' => 'answer answer'
        ];

        $array[] = [
            'id' => 2,
            'question' => 'What is reminder SMS?',
            'answer' => 'answer answer'
        ];

        return response()->json($array);
    }

    public function test()
    { 
  $json = '[
    {
      "id": "23",
      "name": "חבילה כוללת הושבה ",
      "package": {
        "arrivalSms": 3,
        "reminderSms": 1,
        "thanksSms": 1,
        "creditLine": 1,
        "seatingAttendant": 2,
        "callFourthRounds": 1,
        "extraCalls": 0,
        "extraRoundSmsArrival": 0,
        "extraSmsSeating": 0,
        "extraTwoRoundsCalls": 0,
        "extraAttendant": 0
      }
    },
    {
      "id": "24",
      "name": "חבילה מורחבת",
      "package": {
        "arrivalSms": 3,
        "reminderSms": 1,
        "thanksSms": 1,
        "creditLine": 1,
        "callFourthRounds": 1,
        "seatingSms": 1,
        "extraCalls": 0,
        "extraRoundSmsArrival": 0,
        "extraSmsSeating": 0,
        "extraTwoRoundsCalls": 0
      }
    },
    {
      "id": "25",
      "name": "חבילה קלאסית",
      "package": {
        "arrivalSms": 3,
        "reminderSms": 1,
        "thanksSms": 1,
        "creditLine": 1,
        "callFourthRounds": 1,
        "extraCalls": 0,
        "extraRoundSmsArrival": 0,
        "extraSmsSeating": 0,
        "extraTwoRoundsCalls": 0
      }
    },
    {
      "id": "26",
      "name": "חבילה דיגיטלית",
      "package": {
        "arrivalSms": 3,
        "reminderSms": 1,
        "thanksSms": 1,
        "creditLine": 1,
        "extraCalls": 0,
        "extraRoundSmsArrival": 0,
        "extraSmsSeating": 0
      }
    }]';
    $array = json_decode($json, true);
    //var_dump(json_decode($json, true));
    return response()->json($array);
    }

    public function postRequest(Request $request)
    {
        // dd($request->all());
        $validated = $request->validate([
            'PaymentsNumber' => 'required|numeric|min:1|max:6',
            'Sum' => 'required|numeric',
            'invoiceMail' => 'required|string|max:191|email',
            'invoiceMobile' => 'required|string',
            'dealerNumber' => 'required|string|max:191',
            'paymentDescription' => 'required|string|max:191',
            'presenterName' => 'required|string|max:191',
            'uniqId' => 'required|string|max:191',
            'business_name' => 'required|string|max:191',
            'commissionAmount' => 'required|numeric'
        ]);

        //dump($request->all());
        return response()->json($request->all());
    }
}