<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;

class CrmController extends Controller
{
    public function getData(){
    	return Order::with('provider_products')->get();
    }
}
