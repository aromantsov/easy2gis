<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\CrmController;
use App\Http\Controllers\FaqController;
use App\Http\Controllers\AdminController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/test', [OrderController::class, 'test']);

Route::get('/event', [OrderController::class, 'event']);

Route::get('/faq', [OrderController::class, 'faq']);

Route::post('/setorder', [OrderController::class, 'store']);

Route::post('/pay', [ApiController::class, 'pay']); 

Route::post('/createorder', [ApiController::class, 'createOrder']); 

Route::post('/postrequest', [OrderController::class, 'postRequest']);

Route::get('/getproduct', [ApiController::class, 'getProduct']);

Route::get('/getaddproduct', [ApiController::class, 'getAdditionalProduct']);

Route::get('/getpackage', [ApiController::class, 'getPackage']); 

Route::post('/success', [ApiController::class, 'success']); 

Route::get('/crm', [CrmController::class, 'getData']); 

Route::get('/faq', [FaqController::class, 'index']); 

Route::post('/faq', [FaqController::class, 'store']); 

Route::delete('/faq/{id}', [FaqController::class, 'destroy']); 

Route::put('/faq/{id}', [FaqController::class, 'update']); 

Route::get('/getfaq', [FaqController::class, 'faq']);

Route::post('/auth', [AdminController::class, 'auth']); 

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
