<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProviderProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('provider_products', function (Blueprint $table) {
            $table->id();
            $table->integer('order_id');
            $table->boolean('additional');
            $table->integer('minPrice');
            $table->integer('orderOfTheProduct');
            $table->integer('price');
            $table->boolean('notAllowToEditPrice');
            $table->integer('providerMessageTypeId');
            $table->string('dateOfService')->nullable();
            $table->integer('discountToCategoryId')->nullable();
            $table->integer('discountToProductId')->nullable();
            $table->integer('discountToCategory')->nullable();
            $table->integer('discountToProduct')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('provider_products');
    }
}
