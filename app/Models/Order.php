<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function event_types()
    {
    	return $this->belongsTo(EnentType::class, 'eventType', 'id');
    }

    public function provider_products()
    {
    	return $this->hasMany(ProviderProduct::class);
    }
}
