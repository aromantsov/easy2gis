<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->integer('eventType');
            $table->string('ownerLastName');
            $table->string('ownerFirstName');
            $table->string('coOwnerGender');
            $table->string('coOwnerPhone');
            $table->string('coOwnerLastName');
            $table->string('coOwnerFirstName');
            $table->string('hallName');
            $table->string('eventDate');
            $table->string('ownerGender');
            $table->string('ownerPhone');
            $table->string('numberOfGuests');
            $table->string('ownerEmail');
            $table->integer('package_id');
            $table->string('packageName');
            $table->integer('amount');
            $table->integer('pay_id')->nullable();
            $table->string('pay_data')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
