<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddProductNumberOfGuestsAndStuffOfProductColumnsInProviderProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('provider_products', function (Blueprint $table) {
            $table->string('product')->nullable();
            $table->integer('number_of_guests')->default(0);
            $table->integer('stuff_of_product')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('provider_products', function (Blueprint $table) {
            //
        });
    }
}
