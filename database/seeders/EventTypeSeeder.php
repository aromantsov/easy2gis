<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class EventTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('event_types')->insert([
            'eventType' => 'חתונה',
        ]);

        DB::table('event_types')->insert([
            'eventType' => 'בברית',
        ]);

        DB::table('event_types')->insert([
            'eventType' => 'אירוע התרמה',
        ]);

        DB::table('event_types')->insert([
            'eventType' => 'חחינה',
        ]);

        DB::table('event_types')->insert([
            'eventType' => 'חבריתה',
        ]);

        DB::table('event_types')->insert([
            'eventType' => 'חבר מצווה',
        ]);

        DB::table('event_types')->insert([
            'eventType' => 'חבת מצווה',
        ]);

        DB::table('event_types')->insert([
            'eventType' => 'חאחר',
        ]);

        DB::table('event_types')->insert([
            'eventType' => 'חאירוע עסקי',
        ]);

        DB::table('event_types')->insert([
            'eventType' => 'חהתרמה',
        ]);

        DB::table('event_types')->insert([
            'eventType' => 'חהפקה',
        ]);
    }
}
