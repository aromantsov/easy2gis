<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Faq;
use App\Models\User;
use Validator;

class FaqController extends Controller
{

    public function index(Request $request)
    {
    	$headers = apache_request_headers();
    	$bearer = explode('Bearer ', $headers['Authorization']);
    	$token = $bearer[1];
    	$admin = User::where('remember_token', $token)->first();
        if($token == $admin->remember_token){
            return response()->json(Faq::all());
        }
    	return redirect()->back();
    }

    public function faq()
    {
        return response()->json(Faq::all());
    }

    public function store(Request $request)
    {
    	$headers = apache_request_headers();
    	$bearer = explode('Bearer ', $headers['Authorization']);
    	$token = $bearer[1];
    	$admin = User::where('remember_token', $token)->first();
        if($token == $admin->remember_token){
            $validator = Validator::make($request->all(), [
                'question' => 'required|string',
                'answer' => 'required|string',
            ])->validate();

            $article = Faq::create([
                'question' => $request['question'],
                'answer' => $request['answer'],
            ]);

            return $article;
        }
    	return redirect()->back();
    }

    public function destroy($id)
    {
    	$headers = apache_request_headers();
    	$bearer = explode('Bearer ', $headers['Authorization']);
    	$token = $bearer[1];
    	$admin = User::where('remember_token', $token)->first();
        if($token == $admin->remember_token){
            $article = Faq::find($id);
            $article->delete();
            return true;  
        }
    	return redirect()->back();
    }

    public function update(Request $request, $id)
    {
    	$headers = apache_request_headers();
    	$bearer = explode('Bearer ', $headers['Authorization']);
    	$token = $bearer[1];
    	$admin = User::where('remember_token', $token)->first();
        if($token == $admin->remember_token){
    	    $validator = Validator::make($request->all(), [
                'question' => 'required|string',
                'answer' => 'required|string',
            ])->validate();

            $article = Faq::find($id);

            $article->question = $request['question'];
            $article->answer = $request['answer'];
            $article->save();

            return $article;
        }
        return redirect()->back();
    }
}
