<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Order;
use App\Models\ProviderProduct;
use App\Models\Hall;
use Carbon\Carbon;

class ApiController extends Controller
{

    public function getProduct()
    {
    	$source = $this->getPage(getenv('BO_DOMAIN') . '/umbraco/surface/SelfServiceSurface/ProviderProducts');
    	$result = json_decode(json_decode($source, true), true);
    	return response()->json($result);

    }

    public function getAdditionalProduct()
    {
    	$source = $this->getPage(getenv('BO_DOMAIN') . '/umbraco/surface/SelfServiceSurface/AdditionalProviderProducts');
    	$result = json_decode(json_decode($source, true), true);
    	return response()->json($result);

    }

    public function getPackage()
    {
        $source = $this->getPage(getenv('BO_DOMAIN') . '/umbraco/surface/SelfServiceSurface/ProviderPackages');
        $result = json_decode(json_decode($source, true), true);

        if($result){
            for($k = 0; $k < count($result); $k++){ 
                for($j = 0; $j < count($result[$k]['ProviderProducts']); $j++){ //$prods['ProviderProducts']
                    for($i = 0; $i < count($result[$k]['ProviderProducts'][$j]['ProductsToChange']); $i++){
                        if($result[$k]['ProviderProducts'][$j]['ProductsToChange'][$i] === null){
                            unset($result[$k]['ProviderProducts'][$j]['ProductsToChange'][$i]);
                        }
                    }
                    $result[$k]['ProviderProducts'][$j]['ProductsToChange'] = array_values($result[$k]['ProviderProducts'][$j]['ProductsToChange']);
                }
            }
        }
        

        return response()->json($result);
    }

    private function getPage($page){
        $ch = curl_init($page);

        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER , true);

        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    } 

    public function pay(Request $request)
    {
        //header('Access-Control-Allow-Origin': '*');
        $ob = (object)$request->json()->all();

        $json = json_encode($ob);

        $array = json_decode($json, true);

        // $validator = Validator::make($array, [
        //     'ownerLastName' => 'required|string|max:191',
        //     'ownerFirstName' => 'required|string|max:191',
        //     'eventType' => 'required|numeric',
        //     'ownerEmail' => 'required|string|email|min:4|max:191',
        //     'ownerPhone' => 'required|string|digits:10',
        //     'coOwnerPhone' => 'nullable|string|digits:10',
        // ])->validate();

        // if(isset($array['hallName'])){
        //     $hall = Hall::where('name', $array['hallName']['name'])->first();

        //     if(!$hall){
        //         $hall = Hall::create(['google_place_id' => $array['hallName']['google_place_id'], 'name' => $array['hallName']['name'], 'address' => $array['hallName']['address'], 'lat' => $array['hallName']['lat'], 'lng' => $array['hallName']['lng']]);
        //     }
        // }

        $total_price = 0;

        $order = Order::where('id', $array['id'])->update([
            'eventType' => $array['eventType'],
            'ownerLastName' => $array['ownerLastName'],
            'ownerFirstName' => $array['ownerFirstName'],
            'coOwnerGender' => $array['coOwnerGender'],
            'coOwnerPhone' => $array['coOwnerPhone'],
            'coOwnerLastName' => $array['coOwnerLastName'],
            'coOwnerFirstName' => $array['coOwnerFirstName'],
            'hallName' => $hall->id ?? 0,
            'eventDate' => $array['eventDate'],
            'ownerGender' => $array['ownerGender'],
            'ownerPhone' => $array['ownerPhone'],
            'numberOfGuests' => $array['numberOfGuests'],
            'numberOfRecords' => $array['numberOfRecords'] ?? $array['numberOfGuests'],
            'ownerEmail' => $array['ownerEmail'] ?? '',
            'package_id' => $array['package']['Id'],
            'packageName' => $array['package']['PackageName'],
            'amount' => $array['package']['Amount'] ?? 0,
            'crm_status' => 0,
            'rsvp_status' => 0,
        ]);

        $total_price += $array['package']['Amount'];

        //return $array['package']['Amount'];

        $products = [];

        //$package_price = $array['package']['Amount'] ?? 0;
        foreach($array['package']['ProviderProducts'] as $product){
            $product_data['ItemTypeId'] = $product['Id'];
            $product_data['ItemTypeName'] = $product['Product'];
            $product_data['ItemComment'] = '';
            $product_data['ItemDateOfService'] = '';

            //$package_price += $product['Price'] * $product['Quantity'] ?? 1;

            // if($array['package']['Amount']){
            // 	$product_data['ItemPrice'] = 0;
            // 	$product_data['ItemQuantity'] = 0;
            // }else{
            	$product_data['ItemPrice'] = $product['Price'];
            	$product_data['ItemQuantity'] = $product['Quantity'] ?? 0;
            //}

            $prod = ProviderProduct::create([
                'order_id' => $array['id'],
                'item_id' => $product['Id'],
                'additional' => 0,
                'minPrice' => $product['MinPrice'],
                'orderOfTheProduct' => $product['OrderOfTheProduct'],
                'price' => $product['Price'],
                'quantity' => $product['Quantity'] ?? 1,
                'notAllowToEditPrice' => $product['NotAllowToEditPrice'],
                'providerMessageTypeId' => $product['ProviderMessageTypeId'],
                'dateOfService' => $product['DateOfService'],
                'discountToCategoryId' => $product['DiscountToCategoryId'],
                'discountToProductId' => $product['DiscountToProductId'],
                'discountToCategory' => $product['DiscountToCategory'],
                'discountToProduct' => $product['DiscountToProduct'],
                'product' => $product['Product'],
                'number_of_guests' => $product['NumberOfGuests'],
                'stuff_of_product' => $product['StuffOfProduct'],
                'parent_id' => 0
            ]);
            
            if(!$product['ProductsToChange']){
                $total_price += $product['Price'];
            }

            foreach($product['ProductsToChange'] as $product2){

                $product_data['ProviderProduct'][$prod->id]['ItemTypeId'] = $product2['Id'];
                $product_data['ProviderProduct'][$prod->id]['ItemTypeName'] = $product2['Product'];
                $product_data['ProviderProduct'][$prod->id]['ItemPrice'] = $product2['Price'];
                $product_data['ProviderProduct'][$prod->id]['ItemComment'] = '';
                $product_data['ProviderProduct'][$prod->id]['ItemQuantity'] = $product2['Quantity'] ?? 1;
                $product_data['ProviderProduct'][$prod->id]['ItemDateOfService'] = '';

                if($product_data['ItemTypeId'] == $product2['Id']){
                	$product_data['ProviderProduct'][$prod->id]['ItemPrice'] = $array['package']['Amount'] ?? 0;
                }

                ProviderProduct::create([
                    'order_id' => $array['id'],
                    'item_id' => $product2['Id'],
                    'additional' => 0,
                    'minPrice' => $product2['MinPrice'],
                    'orderOfTheProduct' => $product2['OrderOfTheProduct'],
                    'price' => $product2['Price'],
                    'quantity' => $product2['Quantity'] ?? 1,
                    'notAllowToEditPrice' => $product2['NotAllowToEditPrice'],
                    'providerMessageTypeId' => $product2['ProviderMessageTypeId'],
                    'dateOfService' => $product2['DateOfService'],
                    'discountToCategoryId' => $product2['DiscountToCategoryId'],
                    'discountToProductId' => $product2['DiscountToProductId'],
                    'discountToCategory' => $product2['DiscountToCategory'],
                    'discountToProduct' => $product2['DiscountToProduct'],
                    'product' => $product2['Product'],
                    'number_of_guests' => $product2['NumberOfGuests'],
                    'stuff_of_product' => $product2['StuffOfProduct'],
                    'parent_id' => $prod->id
                ]);

                $total_price += $product2['Price'];

                //$products[] = $product_data;
            
            }

            $products[] = $product_data;
            
        }
        if(isset($array['package']['AdditionalProducts'])){

        	// if($product['Quantity'] == 0){
         //        continue;
        	// }
        	foreach($array['package']['AdditionalProducts'] as $product){
                $product_data['ItemTypeId'] = $product['Id'];
                $product_data['ItemTypeName'] = $product['Product'];
                $product_data['ItemComment'] = '';
                $product_data['ItemDateOfService'] = '';

                // if($array['package']['Amount']){
            	   //  $product_data['ItemPrice'] = 0;
            	   //  $product_data['ItemQuantity'] = 0;
                // }else{
            	    $product_data['ItemPrice'] = $product['Price'];
            	    $product_data['ItemQuantity'] = $product['Quantity'] ?? 0;
                //}

                $prod = ProviderProduct::create([
                    'order_id' => $array['id'],
                    'item_id' => $product['Id'],
                    'additional' => 1,
                    'minPrice' => $product['MinPrice'],
                    'orderOfTheProduct' => $product['OrderOfTheProduct'],
                    'price' => $product['Price'],
                    'quantity' => $product['Quantity'] ?? 1,
                    'notAllowToEditPrice' => $product['NotAllowToEditPrice'],
                    'providerMessageTypeId' => $product['ProviderMessageTypeId'],
                    'dateOfService' => $product['DateOfService'],
                    'discountToCategoryId' => $product['DiscountToCategoryId'],
                    'discountToProductId' => $product['DiscountToProductId'],
                    'discountToCategory' => $product['DiscountToCategory'],
                    'discountToProduct' => $product['DiscountToProduct'],
                    'product' => $product['Product'],
                    'number_of_guests' => $product['NumberOfGuests'],
                    'stuff_of_product' => $product['StuffOfProduct'],
                    'parent_id' => 0
                ]);

                $products[] = $product_data;

                if($product['Quantity']){
                    $total_price += $product['Price'] * $product['Quantity'];
                }
            
            }
        }
        
        //mail($array['ownerEmail'], 'Self Service', json_encode($products));
        //session(['order_id' => $order->id]);

    	$data = [];
        
        $data['datepicker'] = date_format(date_create($array['eventDate']), 'd-m-Y');
        $data['timepicker'] = $array['eventTime'];
    	$data['presenterName'] = $array['ownerLastName'] . ' ' . $array['ownerFirstName'] ?? 'testtt';
    	$data['eventType'] =  $array['eventType'] ?? 1;
    	$data['Location'] =  $array['hallName']['name'] ?? 'test'; //$array['hallName']['name']
    	$data['Quantity'] = 1;
    	$data['invoiceMail'] = $array['ownerEmail'] ?? 'test@i.ua';
    	$data['invoiceMail2'] = $array['ownerEmail'] ?? 'test@i.ua';
    	$data['invoiceMobile'] = $array['ownerPhone'] ?? '0500000000';
    	$data['invoiceMobile2'] = $array['coOwnerPhone'] ?? '0520000000';
    	//$data['eventDate'] = $array['eventDate'];
    	$data['descriptionOfBenefits'] = '';
    	$data['paymentnotes'] = 'rsvp';
    	$data['other'] = '';
    	$data['paymentDescription'] = json_encode($products, JSON_UNESCAPED_UNICODE) ?? '[]'; //'[{"ItemTypeId":6,"ItemTypeName":"מבצע - אישורי הגעה בסמס בלבד","ItemPrice":1,"ItemComment":"","ItemQuantity": 1,"ItemDateOfService":"השירות ינתן במועד אשר יקבע על ידי הלקוח ולא יאוחר מיום האירוע כמפורט לעיל."}]';
    	$data['amount'] = $total_price; //$array['package']['Amount'] ?? 0;
    	$data['payments_num'] = 1;
        $data['order_id'] = $array['id'];

        $order = Order::where('id', $array['id'])->update(['amount' => $total_price]);

    	$ch = curl_init('https://www.easy2gift.co.il/umbraco/surface/SelfServiceSurface/PayCG ');

        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: multipart/form-data')); 

        $result = curl_exec($ch);
        curl_close($ch); 

        $orders_crm = Order::with('provider_products')->where('crm_status', 0)->get();

        //return ['json' => $orders_crm];

        $ch = curl_init(env('CRM_DOMAIN') . '/create-lead-self-service?integration_token=' . env('CRM_TOKEN'));

        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER , true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(['json' => $orders_crm]));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 

        $res = curl_exec($ch);
        curl_close($ch);

        $res = json_decode($res);

        if((isset($res->status)) && ($res->status == 'ok')){
            foreach($orders_crm as $order_c){
                //if($order_c->tnx_id){
                    $order_c->crm_status = 1;
                    $order_c->save();
                //}
            }
        }

        $orders_rsvp = Order::with('provider_products')->where('rsvp_status', 0)->where('eventDate', '>', Carbon::now())->get();

        $ch = curl_init(env('RSVP_DOMAIN') . '/api/v1/integration_crm/self_service.json?integeration_token=' . env('RSVP_TOKEN'));

        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)');

        curl_setopt($ch, CURLOPT_RETURNTRANSFER , true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(['json' => $orders_rsvp]));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 

        $result2 = curl_exec($ch);
        curl_close($ch);

        $res2 = json_decode($result2);

        if((isset($res2->status)) && ($res2->status == 'ok')){
            foreach($orders_rsvp as $order_r){
                //if($order_r->tnx_id){
                    $order_r->rsvp_status = 1;
                    $order_r->save();
                //} 
            }
        }

        return $result;
    }

    public function success(Request $request)
    {
        
        $ob = (object)$request->json()->all();

        //return $ob;

        $order = Order::with('provider_products')->where('id', $ob->order_id)->first();
        $order->tnx_id = $ob->TxnID;
        $order->save();

        //$order = json_decode(json_encode($order), true);

        $orders_crm = Order::with('provider_products')->where('crm_status', 0)->get();

        // $ch = curl_init('http://stage-crm.easy2give.co.il/create-lead-self-service?integration_token=G!mudt*yucyd4uouhVHJ$dgu4zk');

        // curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)');
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER , true);
        // curl_setopt($ch, CURLOPT_POST, true);
        // curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(['json' => $orders_crm]));
        // curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 

        // $res = curl_exec($ch);
        // curl_close($ch);

        // $res = json_decode($res);

        // if((isset($res->status)) && ($res->status == 'ok')){
        //     foreach($orders_crm as $order_c){
        //     	if($order_c->tnx_id){
        //             $order_c->crm_status = 1;
        //             $order_c->save();
        //         }
        //     }
        // }

        //dump($result);

        // $orders_rsvp = Order::with('provider_products')->where('rsvp_status', 0)->get();

        // $ch = curl_init('https://stage-l.e2grsvp.com/api/v1/integration_crm/self_service.json?integeration_token=J!HCBV*76845hjds!hf84hg');

        // curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)');

        // curl_setopt($ch, CURLOPT_RETURNTRANSFER , true);
        // curl_setopt($ch, CURLOPT_POST, true);
        // curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(['json' => $orders_rsvp]));
        // curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 

        // $result2 = curl_exec($ch);
        // curl_close($ch);

        // $res2 = json_decode($result2);

        // if((isset($res2->status)) && ($res2->status == 'ok')){
        //     foreach($orders_rsvp as $order_r){
        //         if($order_r->tnx_id){
        //         	$order_r->rsvp_status = 1;
        //             $order_r->save();
        //         } 
        //     }
        // }
        //return json_encode(['json' => $orders_rsvp]);

        //$tnx = Order::where('tnx_id', $ob->TxnID)->get();
        return $order;
    }

    public function createOrder(Request $request)
    {
        $ob = (object)$request->json()->all();

        $json = json_encode($ob);

        $array = json_decode($json, true);

        // if(isset($array['order_id'])){
        //     return $array['order_id'];
        // }

        if($request['eventType'] == 1){
            $validator = Validator::make($array, [
                'ownerLastName' => 'required|string|max:191',
                'ownerFirstName' => 'required|string|max:191',
                'eventType' => 'required|numeric',
                'ownerEmail' => 'required|string|email|min:4|max:191',
                'ownerPhone' => 'required|string|digits:10',
                'coOwnerPhone' => 'required|string|digits:10',
                'coOwnerGender' => 'required',
                'coOwnerLastName' => 'required|string|max:191',
                'coOwnerFirstName' => 'required|string|max:191'
            ])->validate();
        }else{
            $validator = Validator::make($array, [
                'ownerLastName' => 'required|string|max:191',
                'ownerFirstName' => 'required|string|max:191',
                'eventType' => 'required|numeric',
                'ownerEmail' => 'required|string|email|min:4|max:191',
                'ownerPhone' => 'required|string|digits:10',
                'coOwnerPhone' => 'nullable|string|digits:10',
            ])->validate();
        }

        

        if(isset($array['hallName'])){
            $hall = Hall::where('name', $array['hallName']['name'])->first();

            if(!$hall){
                $hall = Hall::create(['google_place_id' => $array['hallName']['google_place_id'], 'name' => $array['hallName']['name'], 'address' => $array['hallName']['address'], 'lat' => $array['hallName']['lat'], 'lng' => $array['hallName']['lng']]);
            }
        }

        $order = Order::create([
            'eventType' => $array['eventType'],
            'ownerLastName' => $array['ownerLastName'],
            'ownerFirstName' => $array['ownerFirstName'],
            'coOwnerGender' => $array['coOwnerGender'],
            'coOwnerPhone' => $array['coOwnerPhone'],
            'coOwnerLastName' => $array['coOwnerLastName'],
            'coOwnerFirstName' => $array['coOwnerFirstName'],
            'hallName' => $hall->id ?? 0,
            'eventDate' => $array['eventDate'],
            'ownerGender' => $array['ownerGender'],
            'ownerPhone' => $array['ownerPhone'],
            'numberOfGuests' => $array['numberOfGuests'],
            'numberOfRecords' => $array['numberOfRecords'] ?? $array['numberOfGuests'],
            'ownerEmail' => $array['ownerEmail'] ?? '',
            'package_id' => 0, //$array['package']['Id'],
            'packageName' => '', //$array['package']['PackageName'],
            'amount' => 0, //$array['package']['Amount'] ?? 0,
            'crm_status' => 0,
            'rsvp_status' => 0,
        ]);


        $orders_crm = Order::with('provider_products')->where('crm_status', 0)->get();

        $ch = curl_init(env('CRM_DOMAIN') . '/api/v1/integration_crm/self_service.json?integeration_token=' . env('CRM_TOKEN'));

        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER , true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(['json' => $orders_crm]));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 

        $res = curl_exec($ch);
        curl_close($ch);

        $res = json_decode($res);

        if((isset($res->status)) && ($res->status == 'ok')){
            foreach($orders_crm as $order_c){
                //if($order_c->tnx_id){
                    $order_c->crm_status = 1;
                    $order_c->save();
                //}
            }
        }

        $orders_rsvp = Order::with('provider_products')->where('rsvp_status', 0)->where('eventDate', '>', Carbon::now())->get();

        $ch = curl_init(env('RSVP_DOMAIN') . '/api/v1/integration_crm/self_service?integeration_token=' . env('RSVP_TOKEN'));

        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)');

        curl_setopt($ch, CURLOPT_RETURNTRANSFER , true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(['json' => $orders_rsvp]));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 

        $result2 = curl_exec($ch);
        curl_close($ch);

        $res2 = json_decode($result2);

        if((isset($res2->status)) && ($res2->status == 'ok')){
            foreach($orders_rsvp as $order_r){
                //if($order_r->tnx_id){
                    $order_r->rsvp_status = 1;
                    $order_r->save();
               // } 
            }
        }

        return $order->id;
    }
 }
